#!/usr/bin/env python

from subprocess import call
from termcolor import colored
from portage import settings
import os, signal, sys



#add kernel update
#add dracut
#ad grub

if os.geteuid() != 0:
	print('\n\n  ####################\n  ## Root required. ##\n  ####################\n')
	sys.exit(0)

def ctrl_c(signal, frame):
	print('\n\n  ########################\n  ## See you next time. ##\n  ########################\n')
	sys.exit(0)

signal.signal(signal.SIGINT, ctrl_c)


sync = [
	"emerge --sync",
	"layman -S",
]

reinstall = [
	"emerge -e @system",
	"emerge -e @world",        
        "perl-cleaner --reallyall"
]

update = [
	"emerge -uND @system",
	"emerge -uND @world",
]

sets = [
	"emerge @live-rebuild",
	"emerge @module-rebuild",
	"emerge @preserved-rebuild",
	"emerge @x11-module-rebuild",
]

consistency = [
	"revdep-rebuild",        
	"etc-update",
]

clean = [
        "haskell-updater",        
        "perl-cleaner --all",
	"eclean-kernel --all --ask",
	"emerge --depclean",
	"eclean -d distfiles",
	"rm -rvf " + os.path.join(settings.get('DISTDIR'), '*'),
]

glsa = [
	"glsa-check -p $(glsa-check -t all)",
	"qcheck -v $(qcheck -B)"
]

groups = [
	['Synchronizate databases',sync],
	['Entire reinstall',reinstall],
	['Incremental Updates',update],
	['Emerge various sets',sets],
	['Consistency checks',consistency],
	['Clean system',clean],
	['Glsa checks',glsa]
]

for group in groups:
	yn = input('Do you want run '+colored(group[0],'yellow')+' [y/n]')
	if yn != 'y':
		continue;
	for command in group[1]:
		yn = input('Do you want run '+colored(command,'green')+' [y/n]')
		if yn != 'y':
			continue;
		call([command], shell=True)
