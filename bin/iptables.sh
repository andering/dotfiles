#!/bin/bash

iptables -F;
iptables -X;
iptables -Z;

#iptables -t nat -F
#iptables -t nat -X
#iptables -t nat -Z

iptables -P INPUT DROP;
iptables -P FORWARD DROP;
iptables -P OUTPUT ACCEPT;

iptables -A INPUT       -i lo -j ACCEPT;
iptables -A OUTPUT      -o lo -j ACCEPT;

iptables -A INPUT -s 183.3.202.174,183.3.202.200,112.85.218.11,112.85.42.107 -j DROP

#udp:vps -> net
iptables -A OUTPUT  -p udp -m multiport --dport 22,53,123 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT;
iptables -A INPUT   -p udp -m multiport --sport 22,53,123 -m conntrack --ctstate ESTABLISHED -j ACCEPT;

#udp:net -> vps
iptables -A INPUT   -p udp -m multiport --dport 22,80,443,3979 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT  -p udp -m multiport --sport 22,80,443,3979 -m conntrack --ctstate ESTABLISHED -j ACCEPT

#tcp:vps -> net
iptables -A OUTPUT  -p tcp -m multiport --dport 22,25,80,443,873,993,8888 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT;
iptables -A INPUT   -p tcp -m multiport --sport 22,25,80,443,873,993,8888 -m conntrack --ctstate ESTABLISHED -j ACCEPT;

#tcp:net -> vps
iptables -A INPUT   -p tcp -m multiport --dport 22,2222,80,443,3979,587 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT  -p tcp -m multiport --sport 22,2222,80,443,3979,587 -m conntrack --ctstate ESTABLISHED -j ACCEPT

#ftp
# The following two rules allow the inbound FTP connection
iptables -A OUTPUT -p tcp --dport 21 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 21 -m state --state ESTABLISHED -j ACCEPT
# The next 2 lines allow active ftp connections
iptables -A INPUT -p tcp --sport 20 -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p tcp --dport 20 -m state --state ESTABLISHED -j ACCEPT
# These last two rules allow for passive transfers
iptables -A INPUT -p tcp --sport 1024: --dport 1024: -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 1024: --dport 1024: -m state --state ESTABLISHED,RELATED -j ACCEPT

#random
iptables -A INPUT       -p tcp -m tcp --dport 113 --tcp-flags FIN,SYN,RST,ACK SYN -j REJECT --reject-with tcp-reset;
iptables -A INPUT       -p icmp -m icmp --icmp-type 0 -j ACCEPT;
iptables -A OUTPUT      -p icmp -m icmp --icmp-type 0 -j ACCEPT;

iptables -A INPUT       -p icmp -m icmp --icmp-type 5 -j ACCEPT;
iptables -A OUTPUT      -p icmp -m icmp --icmp-type 5 -j ACCEPT;
iptables -A INPUT       -p icmp -m icmp --icmp-type 8 -j ACCEPT;
iptables -A OUTPUT      -p icmp -m icmp --icmp-type 8 -j ACCEPT;


iptables -N LOGGING;
iptables -A INPUT       -j LOGGING;
iptables -A OUTPUT      -j LOGGING;
iptables -A LOGGING -j LOG --log-prefix "[iptables] " --log-level 4;
iptables -A LOGGING -j DROP;
