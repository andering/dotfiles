#!/usr/bin/env python
# coding: utf-8

from glob import glob
from random import choice
from time import sleep
import os
import socket
import subprocess
import pprint

from libqtile import layout, widget, bar, hook
from libqtile.widget import base, keyboardlayout
from libqtile.core.manager import Screen, Drag, Click
from libqtile.command import lazy
from libqtile.config import Key, Group, Match
from libqtile.log_utils import logger


mod = 'mod4'

# logger.warning(keyboardlayout)


def move_window_to_screen(screen):
    def cmd(qtile):
        w = qtile.currentWindow
        # XXX: strange behaviour - w.focus() doesn't work
        # if toScreen is called after togroup...
        qtile.toScreen(screen)
        if w is not None:
            w.togroup(qtile.screens[screen].group.name)
    return cmd


params = {
    'font': 'xos4 Terminus',
    'fontsize': 12,
    'foreground': '#e2ccb0',
    'background': '#000000',
    'highlight': '#d88166'
}

fonts = {
    'font': 'xos4 Terminus',
    'fontsize': 12,
    'foreground': '#e2ccb0',
}


keys = [

    Key([mod, "shift"], "Tab", lazy.layout.client_to_next()),
    Key([mod, "shift"], "space", lazy.layout.rotate()),
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "space", lazy.next_layout()),

    Key([mod], "w", lazy.to_screen(0)),
    Key([mod, "shift"], "w", lazy.function(move_window_to_screen(0))),
    Key([mod], "e", lazy.to_screen(1)),
    Key([mod, "shift"], "e", lazy.function(move_window_to_screen(1))),

    Key([mod], "t", lazy.spawn("urxvt")),
    Key([mod], "r", lazy.spawn("dmenu_run -fn '%s:pixelsize=%d'" % (fonts['font'], fonts['fontsize']))),
    #    Key([mod], "r", lazy.spawn("echo 'chromium' | dmenu -fn '%s:pixelsize=%d'" % (fonts['font'], fonts['fontsize']))),
    Key([mod], "Shift_L", lazy.widget["keyboardlayout"].next_keyboard()),
    Key([mod, "control"], "c", lazy.window.kill()),


    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "p", lazy.spawncmd()),
    Key([mod], "g", lazy.switchgroup()),
    #    Key([mod], "i", lazy.next_keyboard()),


    Key([mod], "m", lazy.window.toggle_maximize()),
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    Key([], "Print", lazy.spawn("scrot -e 'mv $f ~/Downloads 2>/dev/null'")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10")),




    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +2%")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -2%")),
    Key(
        [], "XF86AudioMute",
        lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")
    ),
    Key(
        [], "XF86Display",
        lazy.spawn("/home/andering/.config/qtile/sh/xrandr.sh"),
        lazy.restart()
    ),

    Key([mod], "space", lazy.next_layout()),

    Key([mod], "e", lazy.layout.toggle_split()),
    Key([mod], "Left", lazy.layout.swap_left()),

    Key([], "XF86AudioPrev", lazy.spawn("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous")),
    Key([], "XF86AudioNext", lazy.spawn("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"))

]

import inspect


mouse = [
    Drag([mod], "Button3", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button1", lazy.window.set_position(), start=lazy.window.get_position()),
    Click([mod], "Button4", lazy.layout.grow()),
    Click([mod], "Button5", lazy.layout.shrink()),
    Click([mod], "Button2", lazy.layout.normalize()),
]

group_names = [
    (
        "terminal", {
            'matches': [Match(wm_class=["URxvt"])],
            'spawn':'urxvt'
        }
    ),
    (
        "dev", {
            'matches': [Match(wm_class=["Code"])],
            'spawn': 'vscode'
        }
    ),
    (
        "browser", {
            'matches': [Match(wm_class=["Chromium-browser-chromium"])],
            'spawn': 'chromium-browser'
        }
    ),
    (
        "media", {
            'matches': [Match(wm_class=["Spotify"])],
            'spawn': 'spotify'
        }
    ),
    (
        "steam", {
            'matches': [Match(wm_class=["Steam"])]
        }
    ),
    (
        "obs", {
            'matches': [Match(wm_class=["obs"])]
        }
    )
]
groups = [Group(name, **kwargs) for name, kwargs in group_names]


for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
#   keys.append(Key([], "XF86Launch" + str(i + 4), lazy.group[name].toscreen()))
    keys.append(Key([mod, "control"], str(i), lazy.window.togroup(name)))

layouts = [
    layout.MonadTall(ratio=0.720, border_focus=params['foreground'], border_normal=params['background'], border_width=1),
    layout.matrix.Matrix(),
    layout.verticaltile.VerticalTile(border_focus=params['foreground'], border_normal=params['background'], border_width=1)
]


def humanize_bytes(value):
    suff = ["B", "K", "M", "G", "T"]
    while value > 1024. and len(suff) > 1:
        value /= 1024.
        suff.pop(0)
    return "%03d%s" % (value, suff[0])


class Metrics(base.InLoopPollText):

    def __init__(self, **config):
        self.cpu_usage, self.cpu_total = self.get_cpu_stat()
        self.interfaces = {}
        self.idle_ifaces = {}
        base.InLoopPollText.__init__(self, **config)
        self.update_interval = 1

    def get_cpu_stat(self):
        stat = [int(i) for i in open('/proc/stat').readline().split()[1:]]
        return sum(stat[:3]), sum(stat)

    def get_cpu_usage(self):
        new_cpu_usage, new_cpu_total = self.get_cpu_stat()
        cpu_usage = new_cpu_usage - self.cpu_usage
        cpu_total = new_cpu_total - self.cpu_total
        self.cpu_usage = new_cpu_usage
        self.cpu_total = new_cpu_total
        if cpu_total != 0:
            cpu_percents = "%d%%" % (float(cpu_usage) / float(cpu_total) * 100.)
        else:
            cpu_percents = "nan"
        return 'cpu:%s' % cpu_percents

    def get_mem_usage(self):
        info = {}
        for line in open('/proc/meminfo'):
            key, val = line.split(':')
            info[key] = int(val.split()[0])
        mem = info['MemTotal']
        mem -= info['MemFree']
        mem -= info['Buffers']
        mem -= info['Cached']
        if int(info['MemTotal']) != 0:
            mem_percents = '%d%%' % (float(mem) / float(info['MemTotal']) * 100)
        else:
            mem_percents = 'nan'
        return 'mem:%s' % mem_percents

    def get_net_usage(self):
        interfaces = []
        basedir = '/sys/class/net'
        for iface in os.listdir(basedir):
            if iface in ('lo', ):
                continue
            j = os.path.join
            ifacedir = j(basedir, iface)
            statdir = j(ifacedir, 'statistics')
            idle = iface in self.idle_ifaces
            try:
                if int(open(j(ifacedir, 'carrier')).read()):
                    rx = int(open(j(statdir, 'rx_bytes')).read())
                    tx = int(open(j(statdir, 'tx_bytes')).read())
                    if iface not in self.interfaces:
                        self.interfaces[iface] = (rx, tx)
                    old_rx, old_tx = self.interfaces[iface]
                    self.interfaces[iface] = (rx, tx)
                    rx = rx - old_rx
                    tx = tx - old_tx
                    if rx or tx:
                        idle = False
                        self.idle_ifaces[iface] = 0
                        rx = humanize_bytes(rx)
                        tx = humanize_bytes(tx)
                        interfaces.append('%s:%s/%s' % (iface, rx, tx))
            except:
                pass
            if idle:
                interfaces.append(
                    '%s:%-9s' % (iface, ("idle:%02d" % self.idle_ifaces[iface]))
                )
                self.idle_ifaces[iface] += 1
                if self.idle_ifaces[iface] > 30:
                    del self.idle_ifaces[iface]
        return " ".join(interfaces)

    def poll(self):
        stat = [self.get_cpu_usage(), self.get_mem_usage()]
        net = self.get_net_usage()
        if net:
            stat.append(net)
        return " ".join(stat)


def get_bar():
    return bar.Bar([
        #        widget.LaunchBar([('Chromium', 'chromium', 'Chromium')]),
        widget.GroupBox(padding=0, borderwidth=0, disable_drag=True, inactive=params['foreground'], this_current_screen_border=params['highlight'], highlight_method='text', **fonts),
        #        widget.CurrentLayout(padding=0,**fonts),
        widget.Prompt(**fonts),
        widget.Spacer(),
        widget.Clipboard(max_width=100, foreground='FFFF00', timeout=None),
        widget.Spacer(length=25),
#        widget.ThermalSensor(show_tag=True, tag_sensor="CPU", **fonts),
#        widget.ThermalSensor(show_tag=True, tag_sensor="GPU", **fonts),
#        widget.Battery(energy_now_file="charge_now", energy_full_file="charge_full", power_now_file="current_now", update_delay=1, format=" {char} {percent:2.0%} {hour:d}:{min:02d} {watt:.2f} W", **fonts),
        widget.KeyboardLayout(configured_keyboards=['us', 'cz qwerty'], **fonts),
        Metrics(**fonts),
        widget.Sep(foreground="#000000"),
        widget.Clock(format="%a %b %d, %H:%M", **fonts),
    ], 18, background="#000000")


def get_bar2():
    return bar.Bar([
        widget.TaskList(padding=0, highlight_method="text", border=params['highlight'], **fonts),
        widget.Notify(**fonts),
        widget.Volume(
            volume_up_command=['pactl', 'set-sink-volume', '@DEFAULT_SINK@', '+5%'],
            volume_down_command=['pactl', 'set-sink-volume', '@DEFAULT_SINK@', '-5%'],
            mute_command=['pactl', 'set-sink-mute', '@DEFAULT_SINK@', 'toggle'],
            **fonts
        )
    ], 22, background="#000000")


screens = [
    Screen(top=get_bar(), bottom=get_bar2()),
    Screen(top=get_bar(), bottom=get_bar2()),
    Screen(top=get_bar(), bottom=get_bar2())
]


@hook.subscribe.startup_once
def startup_once():
    # subprocess.Popen(["urxvtd"])
    print('')


def get_files():
    patterns = [
        '/home/andering/.config/qtile/wallpapers/*.png',
        '/home/andering/.config/qtile/wallpapers/*.jpg',
    ]
    files = []
    for i in patterns:
        files.extend(glob(i))
    return files


def wallpaper():
    while True:
        subprocess.call(["feh", "--bg-fill", choice(get_files())])
        sleep(300)


def terminal():
    subprocess.call(["urxvt"])


@hook.subscribe.startup
def startup():
    from threading import Thread
    Thread(target=wallpaper).start()
