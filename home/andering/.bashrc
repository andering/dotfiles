# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias fumount='fusermount  -u'
alias suvi=suvi;suvi() { su -c "vi $1"; }


c1='1;33m'
c2='1;32m'
c3='1;32m'
c4='0;34m'

if [[ ${HOSTNAME} == 'vps' ]] ; then
    c2='1;33m'
    c3='1;33m'
elif [[ ${HOSTNAME} == 'pi' ]] ; then
    c2='1;35m'
    c3='1;35m'
fi

if [[ ${EUID} == 0 ]] ; then
    c2='1;31m'
fi

PS1="\[\e[${c1}\][\D{%H:%M}] \[\e]0;\u@\h:\w\007\]\[\e[${c2}\]\u\[\e[${c3}\]@\h\[\e[${c4}\] \w \$\[\e[00m\] "
export PATH="~/bin:$PATH"

