.. include:: styles/basic.rst

|hr|

Skeleton:
----------------
- user role (can, decorator, nice example, permission_required)
- `validate_` and `test_` style and santitize_
- _post.html - partial template
- to_json() and from_json()
- werkzeuq profiler , flask profiler
- unittest vs pytest
- Add coverage to skeleton
- validating
- Create blueprint in __init__ page 92
- Cheatsheets
- session server vs client side
- production server
- firebase, foreman & travis -> google cloud?
- Entity manager - back in the game (alchemy example)?
- Flask-mail, Flask-login, Flask-restful
- Flask-restful, Flask-oauth, Flask-kvsession, Flask-httpauth
- https://github.com/alecthomas/injector

|hr|

Fce
---------

=================   ==========================================================================================================================================================
jsonify()           function in flask returns a flask.Response() object that already has the appropriate content-type header 'application/json' for use with json responses.
json.dumps()        method will just return an encoded string, which would require manually adding the MIME type header.
=================   ==========================================================================================================================================================

|hr|

Contexts
-------------

============    ====================    ========================================================================================================================================
current_app     application context     the application instance for the active application
g               application context     an object that the application can use for temporary storage during the handling of a request. This variable is reset with each request
request         request context	        the request object, which encapsulates the contents of a HTTP request sent by the client
session         request context	        the user session, a dictionary that the application can use to store values that are ‘remembered’ between requests
============    ====================    ========================================================================================================================================

|hr|

Hooks
-------------
=====================   ===============================================================================================
before_first_request    register a function to run before first request is handled
before_request		    register a function to run before each request
after_request		    register a function to run after each request, if no unhandled exceptions occurred
teardown_request		register a function to run after each request, even if unhandled exceptions occurred
=====================   ===============================================================================================

|hr|

Return codes
-------------

====  =====================   ==========================================================================================
200   ok                      the request was completed successfully
201   created                 the request was completed successfully and a new resource was created as a result
400   bad request             the request is invalid or inconsistent
401   unauthorized            the request does not include authentication information
403   forbidden               the authentication credentials sent with the request are insufficient for the request
404   not found               the resource referenced in the URL was not found
405   method not allowed      the request method requested is not supported for the given resource
500   internal server error   an unexpected error has occured while processing the request
====  =====================   ==========================================================================================

|hr|

Methods
-------

=======  ==============================================  =======================================================================================================================================================================================
GET      individual resource URL                         obtain the resource
GET      resource collection URL                         obrain the collection of resources (or one page from it if the server implements pagination).
POST     resource collection URL                         create a new resource and add it to the collection. The server chooses the URL of the new resource and returns it in a Location header in the response
PUT      individual resource URL                         modify an existing redource. Alternatively this method can also be used to create a new resource when the client can choose the resource URL
DELETE   individual resource URL                         delete a resource
DELETE   resource collection URL                         delete all resources in the collection
=======  ==============================================  =======================================================================================================================================================================================

|hr|