
.. role:: red
    :class: red

.. raw:: html

    <style>
        body {font-size:14px;background:white;color:black;}
        .red {color:red;}
        h1 {font-size: 20px;text-transform:uppercase;font-weight: 600;}
        hr { background:black; height: 1px; margin: 48px 0; display:block;}
    </style>

.. |hr| raw:: html

  <hr/>