.. include:: styles/basic.rst

|hr|

Libs
=========

- PVM - Cpython, Stackless python, Pypy, Cython
- Pystone.py - https://speed.python.org/ - http://speed.pypy.org/
- fce: dir(), help(),  list(), range(), sorted(), vars(), format(),range(),zip(),enumerate(),map(), filter(), reduce(), Isinstance()
- unittest,os,sys,math, random,re, struct, numpy,builtins, builtins(__builtin__), beautifultable, pickle, shelve, json, struct, copy, types, dbm,PyDoc, docstrings,PyChecker,PyLint,PyUnit,Profilers,Debuggers, os.popen, Argparse Import, from, reload, traceback, collections, namedtuple
- Source(m.py) > Byte code(m.pyc) > Virtual machine(PVM) (byte code is saved in files (__cache__) only for included files)
- assert statements may be removed from a compiled program’s byte code if the -O Python command-line flag is used, Assertions are typically used to verify program conditions during developmen
- More formally, if a class defines a __getitem__ index overload method and X is an instance of this class, then an index expression like X[I] is roughly equivalent to type(X).__getitem__(X, I) for new-style classes - the latter beginning its search in the class, and thus skipping a __getattr__ step from the instance for an undefined name. To work around this, you must redefine such methods in wrapper classes, either manually, with tools, or by definition in superclasses

|hr|

Thoughts
=========

- "If it walks like a duck and it quacks like a duck, then it must be a duck" With normal typing, suitability is determined by an object's type. In duck typing, an object's suitability is determined by the presence of certain methods and properties, rather than the type of the object itself. Important in duck typing is what the object can actually do, rather than what the object is. By emphasizing interfaces rather than specific types, well-designed code improves its flexibility by allowing polymorphic substitution. Duck-typing avoids tests using type() or isinstance(). Instead, it typically employs hasattr() tests or EAFP programming. By checking for specific types in your code, you effectively break its flexibility not constraining code to specific types makes that code automatically applicable to many types in Python, we code to object interfaces (operations supported), not to types. you should write your code to expect only an object interface, not a specific data type “Polymorphism” means that the meaning of an operation (like a + ) depends on the objects being operated on. Of course, some programs have unique requirements, and this polymorphic model of programming means we have to test our code to detect errors,  rather than providing type declarations a compiler can use to detect some types of errors for us ahead of time.  In exchange for an initial bit of testing, though, we radically reduce the amount of code we have to write and radically increase our code’s flexibility. As you’ll learn, it’s a net win in practice.
- Interpreted dynamic general purpose language, dynamic-strongly typed
- flat is generally better than nested
- if something is difficult for you to understand, it’s probably not a good idea.
- When inheritance gets harder consider delegation even if they are inner parts
- Encapsulation means packaging in Python - that is, hiding implementation details behind an object’s interface. It does not mean enforced privacy. Python is more about enabling than restricting.
- Calling superclass constructors from redefinitions this way turns out to be a very common coding pattern in Python. super is not widely recognized as “best practice” in Python today, for completely valid reasons.
- I was going to suggest the same thing, but you beat me to it. This is much more Pythonic because it gives the proper error when the method isn't implemented, but doesn't require a method to be implemented if it won't be used in the context of the subclass.
- End on expection is no good, exception can be also used as signal carrier
- Exceptions, DI, Atomicity, Consistency, Autowritting, Testing, Validation, Caching, Lazy loading, Regexp, Threading, Annotation, interspection
- Model is consistent in all states
- Single responsibility principle
- Fon't repeat yourself
- Convention over configuration ??
- Beautiful is better than ugly, Explicit is better than implicit, Simple is better than complex, Complex is better than complicated, Flat is better than nested, Sparse is better than dense.
- Readability counts.
- Special cases aren't special enough to break the rules. Although practicality beats purity.
- Errors should never pass silently. Unless explicitly silenced.
- In the face of ambiguity, refuse the temptation to guess.
- There should be one-- and preferably only one --obvious way to do it. Although that way may not be obvious at first unless you're Dutch.
- Now is better than never, Although never is often better than *right* now.
- If the implementation is hard to explain, it's a bad idea. If the implementation is easy to explain, it may be a good idea.
- Loose coupling is achieved by means of a design that promotes single-responsibility and separation of concerns.
- EAFP - Easier to ask for forgiveness than permission.
- “expression” is a combination of values and functions that are combined and interpreted by the compiler to create a new value, “statement” which is just a standalone unit of execution and doesn't return anything. expression - A piece of syntax which can be evaluated to some value. In other words, an expression is an accumulation of expression elements like literals, names, attribute access, operators or function calls which all return a value. In contrast to many other languages, not all language constructs are expressions statement - A statement is part of a suite (a “block” of code). A statement is either an expression or one of several constructs with a keyword, such as if, while or for.
- Higher-order function - takes one or more functions as arguments (i.e. procedural parameters), returns a function as its result.
- contains a definition for a function f(), and x is an instance of C, calling x.f(1) is equivalent to calling C.f(x, 1)., x[i]  =  type(x).__getitem__(x, i)
- NotImplemented - In user defined base classes, abstract methods should raise this exception when they require derived classes to override the method, or while the class is being developed to indicate that the real implementation still needs to be added.
- When a class definition is executed, the following steps occur: 1. MRO entries are resolved 2. the appropriate metaclass is determined 3. the class namespace is prepared 4. the class body is executed 5. the class object is created
- variables belong to instance or class
- To avoid race conditions like symlink attacks and issues with temporary files and directories, it is more reliable (and also faster) to manipulate file descriptors instead of file names
- In contrast to variable declarations in statically typed languages, the goal of annotation syntax is to provide an easy way to specify structured type metadata for third party tools and libraries via the abstract syntax tree and the __annotations__ attribute.
- design commands to be safe executed more times
- charset is the set of characters you can use, encoding is the way these characters are stored into memory
- Dependency injection as a formal pattern is less useful in Python than in other languages, primarily due to its support for keyword arguments, the ease with which objects can be mocked, and its dynamic nature. That said, a framework for assisting in this process can remove a lot of boiler-plate from larger applications. That's where Injector can help. It automatically and transitively provides keyword arguments with their values.
