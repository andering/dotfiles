
#Alternative to interface:
class Something(object):
    def some_method(self):
        raise NotImplementedError()
    def some_other_method(self, some_argument):
        raise NotImplementedError()

# "Best practice" key sorting
for k in sorted(D): print(k, D[k], end=' ')

#Assignments:
nudge, wink = wink, nudge (tupple swap)
(a, b, c) = "ABC"
a, *b, c = seq
for (a, *b, c) in [(1, 2, 3, 4), (5, 6, 7, 8)]:
sys.stdout = FileFaker()

#Factory:
def maker(N):
    def action(X):
        return X ** N
return action

#Calling:
M.name
M.__dict__['name']
sys.modules['M'].name
getattr(M, 'name')

#Decorator with self:
def decorator(func):
    def _decorator(self, *args, **kwargs):
        return func(self, *args, **kwargs)
    return _decorator

#Class decorator for fce & methods:
class tracer(object):
    def __init__(self, func):
        self.calls = 0
        self.func = func
    def __call__(self, *args, **kwargs):
        self.calls += 1
        print('call %s to %s' % (self.calls, self.func.__name__))
        return self.func(*args, **kwargs)
    def __get__(self, instance, owner):
        def wrapper(*args, **kwargs):
            return self(instance, *args, **kwargs)
        return wrapper


#Async:
async def ticker(delay, to):
    for i in range(to):
        yield i
        await asyncio.sleep(delay)

result = [i async for i in aiter() if i % 2]
result = [await fun() for fun in funcs if await condition()]
