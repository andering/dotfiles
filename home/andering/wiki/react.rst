.. include:: styles/basic.rst

Think in React (https://reactjs.org/docs/thinking-in-react.html):

Break The UI Into A Component Hierarchy
Build A Static Version in React
Identify The Minimal (but complete) Representation Of UI State
Identify Where Your State Should Live
Add Inverse Data Flow

https://facebook.github.io/create-react-app/docs/getting-started
https://developer.chrome.com/extensions/migrating_to_service_workers
https://reactjs.org/docs/typechecking-with-proptypes.html

Standard:
Always start component names with a capital letter.
Since JSX is closer to JavaScript than to HTML, React DOM uses camelCase property naming convention instead of HTML attribute names
The Data Flows Down
In React, sharing state is accomplished by moving it up to the closest common ancestor of the components that need it. This is called “lifting state up”.

Resources:
https://jaredpalmer.com/formik


|hr|



- Which styling technique to use ?
- static vs dynamic type check javascript
- babel general and https://www.styled-components.com/docs/tooling#babel-plugin
- https://github.com/Igorbek/typescript-plugin-styled-components
- https://www.styled-components.com/docs/tooling
- styled-component with babel - This plugin adds support for server-side rendering, minification of styles, and a nicer debugging experience.

|hr|

- client vs server side rendering
- react life cycle (hooks)
- SSR https://material-ui.com/guides/server-rendering/
- https://material-ui.com/guides/minimizing-bundle-size/
- https://github.com/QuickBase/babel-plugin-styled-components-css-namespace
- https://www.styled-components.com/docs/tooling#typescript-plugin
- https://babeljs.io/docs/en/babel-preset-typescript
- https://www.styled-components.com/docs/tooling
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
- https://github.com/styled-components/stylelint-config-styled-components
- https://codeburst.io/next-js-ssr-vs-create-react-app-csr-7452f71599f6

|hr|

- https://relay.dev/
- https://docs.graphene-python.org/en/latest/relay/mutations/
- https://graphql.org/learn/best-practices/
- https://graphql.org/learn/global-object-identification/